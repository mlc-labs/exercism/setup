## Exercism

Guia para instalar el cliente del proyecto y algunos pasos necesarios para hacer el setup del ambiente de desarrollo

### Exercism Cli

guia oficial: http://exercism.io/clients/cli/linux
link: https://github.com/exercism/cli/releases/latest

#### Instalacion

```bash
export release_link=$(curl https://github.com/exercism/cli/releases/latest  | grep -Eoi '<a [^>]+>' | grep -Eo 'href="[^\"]+"' | grep -Eo '(http|https)://[^"]+' | sed "s/tag/download/")
wget "$release_link/exercism-linux-64bit.tgz"
tar -xzvf exercism-linux-64bit.tgz
sudo mv exercism /usr/local/bin
```

#### Verificar instalacion

```bash
exercism version
```

#### Configuracion

Se debe definir el token del usuario y el  directorio de trabajo

```bash
exercism configure -t YOUR_API_KEY -w ~/some/other/place
```

#### Iniciarse

Para iniciar los ejercicios de un lenguaje

```bash
exercism fetch rust
```

Como saber cuales lenguajes estan habilitados?

```bash
exercism tracks
```


